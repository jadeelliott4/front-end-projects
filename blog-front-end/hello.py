from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime




app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/elliotj/PycharmProjects/newTester/blog-server/blog.db'



db = SQLAlchemy(app)


class Blogpost(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.String(50))
	subtitle = db.Column(db.String(50))
	author = db.Column(db.String(20))
	date_posted = db.Column(db.DateTime)
	content = db.Column(db.Text)



class Comment1(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	timestamp = db.Column(db.DateTime)
	username = db.Column(db.String(50))
	content = db.Column(db.Text)
	post_id = db.Column(db.Integer, db.ForeignKey('blogpost.id'))


db.create_all()


@app.route('/')
def index():
	post = Blogpost.query.all()
	return render_template('index.html',post=post)


@app.route('/about')
def about():
	return render_template('about.html')

@app.route('/add')
def add():
	return render_template('bloginput.html')



@app.route('/post/<int:post_id>')
def post(post_id):
	post = Blogpost.query.get_or_404(post_id)
	date_posted = post.date_posted.strftime('%B %d, %Y')
	comment = Comment1.query.filter_by(post_id=post_id)



	return render_template('post.html',post=post, date_posted=date_posted,comment = comment,post_id=post_id)



@app.route('/contact')
def contact():
	return render_template('contact.html')



@app.route('/addpost',methods=['POST'])
def addpost():
	title = request.form['title']
	subtitle = request.form['subtitle']
	author = request.form['author']
	content = request.form['content']

	post = Blogpost(title=title, subtitle=subtitle,author=author,content=content, date_posted = datetime.now())
	date_posted = post.date_posted.strftime('%B %d, %Y')
	db.session.add(post)
	db.session.commit()

	return render_template('post.html', post=post, date_posted=date_posted, post_id=post.id)


@app.route('/addcomment',methods=['POST'])
def addcomment(post_id=None):
	username = request.form['username']
	content = request.form['content']
	if username == "":
		username = "Anonymous"

	post_id = request.form['post_id']


	comment = Comment1(timestamp=datetime.now(), username = username,content = content,post_id = post_id)

	db.session.add(comment)
	db.session.commit()

	return post(post_id)


if __name__ == '__main__':
	app.run(debug=True)